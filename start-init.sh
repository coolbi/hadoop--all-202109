#!/bin/bash
export FLUME_HOME=/opt/module/flume
export FLUME_CONF_DIR=$FLUME_HOME/conf
export HADOOP_HOME=/opt/module/hadoop
export HIVE_HOME=/opt/module/hive
export HBASE_HOME=/opt/module/hbase
export PATH=$PATH:$HIVE_HOME/bin
export PATH=$PATH:/opt/module/hbase/bin
export PATH=$PATH:$FLUME_HOME/bin
export JAVA_HOME=/usr/lib/jdk
export PATH=$JAVA_HOME/bin/:$PATH

export HADOOP_HOME=/opt/module/hadoop
export PATH=$PATH:$HADOOP_HOME/bin

source /etc/profile
yes | cp -rf ./config/01/hadoop/* $HADOOP_HOME/etc/hadoop/
yes | cp -rf ./config/01/hbase/* $HBASE_HOME/conf/
yes | cp -rf ./config/01/hive/* $HIVE_HOME/conf/
yes | cp -rf ./config/01/spark/* $SPARK_HOME/conf
yes | cp -rf ./config/01/zookeeper/* /opt/module/zookeeper/conf
yes | cp -rf  ./config/01/flume/* $FLUME_HOME/conf
yes | cp -rf  ./config/01/sqoop/* $SQOOP_HOME/conf
yes | cp -rf  ./config/01/kafka/* $KAFKA_HOME/conf


scp -r ./config/02/hadoop/* hadoop02:$HADOOP_HOME/etc/hadoop/
scp -r ./config/02/hbase/* hadoop02:$HBASE_HOME/conf/
scp -r ./config/02/spark/* hadoop02:$SPARK_HOME/conf
scp -r ./config/02/flume/* hadoop02:$FLUME_HOME/conf
scp -r ./config/02/kafka/* hadoop02:$KAFKA_HOME/conf
scp -r ./config/02/zookeeper/* hadoop02:/opt/module/zookeeper/conf

scp -r ./config/03/hadoop/* hadoop03:$HADOOP_HOME/etc/hadoop/
scp -r ./config/03/hbase/* hadoop03:$HBASE_HOME/conf/
scp -r ./config/03/spark/* hadoop03:$SPARK_HOME/conf
scp -r ./config/03/flume/* hadoop03:$FLUME_HOME/conf
scp -r ./config/03/kafka/* hadoop03:$KAFKA_HOME/conf
scp -r ./config/03/zookeeper/* hadoop03:/opt/module/zookeeper/conf

rm -rf ~/.bash_history
history -c

#修改hadoop/sbin/start-yarn.sh,stop-yarn.sh,start-dfs.sh,stop-dfs.sh
export HDFS_NAMENODE_USER=ld
export HDFS_DATANODE_USER=ld
export HDFS_SECONDARYNAMENODE_USER=ld
export YARN_RESOURCEMANAGER_USER=ld
export YARN_NODEMANAGER_USER=ld

scp -r /opt/module/hadoop/sbin/* hadoop02:/opt/module/hadoop/sbin